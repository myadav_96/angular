import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormDataService } from "../form-data.service";

@Component({
  selector: 'app-viewform',
  templateUrl: './viewform.component.html',
  styleUrls: ['./viewform.component.css']
})
export class ViewformComponent implements OnInit {
  data;
  constructor(private route: Router,private serviceFormData: FormDataService) { }

  ngOnInit() {
    //this.data = JSON.parse(localStorage.getItem("data"));
    this.data = JSON.parse(this.serviceFormData.getData());
    console.log(this.data);
  }
  
  editForm() {
    this.route.navigate([""])
  }
  formSubmitted() {
    if(this.data){
    alert("Thank you. Your form has been submitted.");
    //window.location = "/some/url";
    }
    else {
      alert("Invalid Submission. Fill the form first.");
    }
  }
}
