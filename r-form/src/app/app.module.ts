import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NameEditorComponent } from './name-editor/name-editor.component';

import { ReactiveFormsModule } from '@angular/forms';
import { ProfileEditorComponent } from './profile-editor/profile-editor.component';
import { RouterModule, Routes  } from '@angular/router';
import { ViewformComponent } from './viewform/viewform.component'
import { FormDataService } from "./form-data.service";

const routes : Routes = [{
  path : '',
  component : ProfileEditorComponent },
  {
    path : 'viewform',
    component : ViewformComponent
  }

]

@NgModule({
  declarations: [
    AppComponent,
    NameEditorComponent,
    ProfileEditorComponent,
    ViewformComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [FormDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
