import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { FormDataService } from "../form-data.service";
@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})

export class ProfileEditorComponent implements OnInit {
  registerForm: FormGroup;
  showHidePassword: string;
  showHideConfirmPassword:string;
  submitted = false;
  genders = ['male', 'female'];
  userobj;
  data;

  constructor(private formBuilder: FormBuilder, private route: Router,private serviceFormData: FormDataService) {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern(/^[A-Za-z]+$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^[A-Za-z]+$/)]],
      gender: ['male', Validators.required],
      contact: ['', [Validators.required, Validators.maxLength(10), Validators.pattern(/^(\+\d{1,3}[- ]?)?\d{10}$/)]],
      password: ['', [Validators.required, Validators.minLength(6),Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6),Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,15}/)]],
      empid: ['', [Validators.required, Validators.maxLength(4)]]
    });

  }

  ngOnInit() {
    this.showHidePassword = 'password';
    this.showHideConfirmPassword = 'password';
    // if (JSON.parse(localStorage.getItem("data"))) {
      // this.userobj = JSON.parse(localStorage.getItem("data"));
      if (this.serviceFormData.getData()){
      this.userobj = JSON.parse(this.serviceFormData.getData());
      this.registerForm.patchValue({
        firstName: this.userobj.firstName,
        lastName: this.userobj.lastName,
        gender: this.userobj.gender,
        contact: this.userobj.contact,
        password: this.userobj.password,
        //confirmpassword: this.userobj.confirmpassword,
        empid: this.userobj.empid
      });
    }
  //   window.onbeforeunload = function () {
  //     localStorage.clear();
  //     return '';
  //   };
  }

  show() {
    if(this.showHidePassword === 'password')
    {
      this.showHidePassword = 'text';
    }
    else {
      this.showHidePassword = 'password';
    }
   
  }

  showConfirm() {
    if(this.showHideConfirmPassword === 'password')
    {
      this.showHideConfirmPassword = 'text';
    }
    else {
      this.showHideConfirmPassword = 'password';
    }
   
  }

  validpass: boolean = false;
  passvalid() {
    this.validpass = this.registerForm.value.password === this.registerForm.value.confirmpassword;
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid || !this.validpass) {
      return;
    }
    this.route.navigate(["/viewform"])
    //localStorage.setItem("data", JSON.stringify(this.registerForm.value));
    this.data = JSON.stringify(this.registerForm.value);
    this.serviceFormData.check(this.data);
  }
}