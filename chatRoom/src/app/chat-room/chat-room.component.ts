import { Component, OnInit } from '@angular/core';
import {ChatroomService} from '../chatroom.service';
import {Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent implements OnInit {

  channel:string="";
  newChannel:string;
  myMsg:string;
  allMsg:any;
  allChannels:any;
  userData:any;
  interval:any;
  channelInterval:any;
  regEx:any;
  searchChannel=[];
  userChannels =[];
  channelHeading:string="general";

  constructor(private chatRoomService:ChatroomService,private route:Router) {

    this.userData = JSON.parse(localStorage.getItem("userData"))

    //creats user at login
    this.chatRoomService.createUser(this.userData.email, this.userData.name).subscribe(res => {
      console.log(res);
    },
      err => {
        console.log(err)
      });

    //adds user to general channel at login.  
    this.chatRoomService.addUserToGeneral(this.userData.email).subscribe(res => {
      console.log(res);
    },
      err => {
        console.log(err)
      });

    //gets all channels and stores it in var allChannels.
    this.chatRoomService.getAllChannels().subscribe((res)=>{
      this.allChannels = res;
      localStorage.setItem("allChannels",JSON.stringify(res));
    },
      err=>{
        console.log(err);
      });
  
    //gets users subscribed channels.
    this.channelInterval = setInterval(()=>{
    this.chatRoomService.getUserChannels(this.userData.email).subscribe(res => {
      console.log(res);
      localStorage.setItem("userChannels",JSON.stringify(res));
     
    },
      err => {
        console.log(err)
      });
    },1000);
   }
   
   ngOnInit() {

    //gets all messages from twilio
    this.interval = setInterval(() => {
      this.chatRoomService.getAllMsg().subscribe(res => {
        console.log(res);
        this.allMsg = res.messages;
        this.userChannelsList();
      },
        err => {
          console.log(err)
        });
    }, 1000)
  }
 
  ngOnDestroy() {
    //clears the message interval.
    clearInterval(this.interval);
    clearInterval(this.channelInterval);
  }

  //creates new service.
  createService(){
        this.chatRoomService.createService().subscribe(res=>{
          console.log(res)
        },
        err=>{
          console.log(err);
        });
  }

//finds the existing channels in the twilio.
  findChannel(){
    if (this.channel.length >= 3) {
    this.regEx = new RegExp(this.channel, "i");
    this.searchChannel.length = 0;
    for (let str of this.allChannels.channels) {
      if (this.regEx.test(str.unique_name)) {
        this.searchChannel.push(str.unique_name)
        console.log(str.unique_name);
      }
    }
    console.log(this.allChannels.channels);
    }else{
      this.searchChannel.length = 0;
    }
  }

  //adds a new user to a channel.
  addUser(channel){
    let addUser = window.confirm("Are you sure you want to join " + channel + "?");
    if (addUser) {
      this.chatRoomService.addUser(channel, this.userData.email).subscribe(res => {
        console.log(res);
        this.searchChannel.length = 0;
        this.channel = "";
      },
        err => {
          alert("You are already a member of " + channel);
          this.searchChannel.length = 0;
          this.channel = "";
          console.log(err)
        })
    }
}

//creates new channel.
  createChannel(){
    console.log("New Channel: "+this.newChannel);
    if (this.newChannel!==undefined){
    let create = confirm('Create new channel '+ this.newChannel + "?")
    if(create){
      this.chatRoomService.createChannel(this.newChannel.toLowerCase()).subscribe(res=>{
      console.log("Channel Id "+JSON.stringify(res.sid));
      this.chatRoomService.addUser(this.newChannel, this.userData.email).subscribe(res => {
        console.log(res);
        this.searchChannel.length = 0;
        this.channel = "";
      });
      alert(this.newChannel+" has been created.")
      this.newChannel = "";
    }, 
  err=>{
    console.log(err);
  });}else{
    return;
  }}else{
    alert("Channel name can't be null.");
    return;
  }
  }

//sends messages to the channel using channel id.
  sendMsg(){
    if (this.myMsg.length!=0) {
    this.chatRoomService.sendMsg(this.myMsg,this.userData.email).subscribe(res=>{
      console.log(res);
      this.myMsg="";
    },
  err=>{
    console.log(err);
  });
  }else{
    return;
    }
  }

  //sends message on Enter.
  keyDownFunction(event) {
    if(event.keyCode == 13) {
      this.sendMsg();
    }
  }

  //changes the value of channel in localstorage to current channel. 
  swapChannel(channel) {
    this.channelHeading = channel;
    localStorage.setItem("channel", channel);
    console.log(channel);
  }

  //prints all the current users channels.
  userChannelsList(){
    this.userChannels.length=0;
    let allChannels=JSON.parse(localStorage.getItem("allChannels"));
    let userChannels = JSON.parse(localStorage.getItem("userChannels"));
    for(let all of allChannels.channels){
      for(let user of userChannels.channels){
        if(all.sid == user.channel_sid){
          this.userChannels.push(all.unique_name);
        }
      }
    }
    this.userChannels.reverse();
  }

  //on logout clears all the user data and navigates to signIn.
  logoutUser(){
    let logout = confirm("Do you really want to logout?");
    if(logout){
      localStorage.clear();
      localStorage.clear();
      this.route.navigate(['/']);
    }else{
      return;
    }
  }
}