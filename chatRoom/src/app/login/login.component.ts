import { Component, OnInit } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { Router} from '@angular/router';
import {ChatroomService} from '../chatroom.service'
import { AuthService,FacebookLoginProvider,GoogleLoginProvider } from 'angular-6-social-login-v2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  constructor( private socialAuthService: AuthService, private route: Router,private chatRoomService:ChatroomService ) {}
  
//social logIn match and authentication.
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        //stores loggedIn userdata in localStorage.
        localStorage.setItem("userData",JSON.stringify(userData));
        localStorage.setItem("loggedIn1","userLoggedIn1");
        localStorage.setItem("loggedIn2","userLoggedIn2");
        this.route.navigate(['/chat-room']);    
      }
    );
  }

  ngOnInit(){
    //stores general value in key channel.
    localStorage.setItem("channel", "general");
  }
}
