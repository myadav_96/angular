import { Injectable } from '@angular/core';
import { CanActivate, Router } from '../../node_modules/@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(private route:Router) { }
  
  //checks the login activity of the user if user accesses the chatroom directly from any of the tab.
  canActivate(){
    if(localStorage.getItem("loggedIn2")=="userLoggedIn2"){
      return true;
    }
    alert('Please login to continue.');
    this.route.navigate(['/']);
    return false;
  }
}