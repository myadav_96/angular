import { Injectable } from '@angular/core';
import { Router } from '../../node_modules/@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthService {

  constructor(private route:Router) { }
  //checks if the user is already loggedIn in any of the tabs.
  canActivate(){
    if(localStorage.getItem("loggedIn1")=="userLoggedIn1"){
      alert('You are already logged In.');
      this.route.navigate(['/chat-room']);
      return false;
    }
    return true;
  }
}
