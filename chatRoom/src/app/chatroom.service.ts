import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from '../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatroomService {
  //required details for twilio chat services.
  idService:string="ISb5cfbd2ec2984b35aab5f1bf1c713852";
  chatService:string = "https://chat.twilio.com/v2/Services/"+this.idService;

  //header containing authentication for twilio api.
  httpOpt={
   headers : new HttpHeaders({ 
    'Content-Type' :'application/x-www-form-urlencoded',
    "Authorization" : "Basic QUM0MTZmZTQ2YWJmNjNhMTM5ZDU1NTBjZmE3MTVlNzcwNTo5NzYwMzBjYjdkMmQzMmFiMDE1Y2YyYjBjNTdmNDg3Yw=="
  })}

  constructor(private http:HttpClient) {}
  
  //creates in service in twilio.
  createService():Observable<any>{
    return this.http.post("https://chat.twilio.com/v2/Services","FriendlyName=chatRoom",this.httpOpt);
  }

  //creates new channel.
  createChannel(newChannel):Observable<any>{
    return this.http.post<any>(this.chatService+"/Channels/","UniqueName="+newChannel+"&ServiceSid="+this.idService, this.httpOpt);
      }
 
  //creates new user with unique identity.
  createUser(identity,fName): Observable<any> {
    return this.http.post<any>(this.chatService+"/Users","FriendlyName="+fName+"&Identity="+identity+"&ServiceSid="+this.idService, this.httpOpt);
   
  }  

  //adds a user to general channel.
  addUserToGeneral(id): Observable<any> {
    return this.http.post<any>(this.chatService+"/Channels/general/Members","UniqueName=general&Identity="+id+"&ServiceSid="+this.idService, this.httpOpt);

  }

  //adds a user to any of the available channels.
  addUser(channelName,email): Observable<any> {
    return this.http.post<any>(this.chatService+"/Channels/"+channelName+"/Members","UniqueName ="+channelName+"&Identity="+email+"&ServiceSid="+this.idService, this.httpOpt);

  }

  //gets all the channels of a user.
  getUserChannels(userEmail): Observable<any> {
    return this.http.get(this.chatService+"/Users/"+userEmail+"/Channels/",this.httpOpt);
  }

  //sends a message to a particular channel.
  sendMsg(myMsg,user):Observable<any>{
    let channel = localStorage.getItem("channel");
    return this.http.post(this.chatService+"/Channels/"+channel+"/Messages","ChannelSid="+channel+"&ServiceSid="+this.idService+"&Body="+myMsg+"&From="+user,this.httpOpt); 
  }

  //gets all the messages from twilio for a particular channel.
  getAllMsg():Observable<any>{
    let channel = localStorage.getItem("channel");
    return this.http.get(this.chatService+"/Channels/"+channel+"/Messages",this.httpOpt);
  }

  //gets all the channles of a service from twilio.
  getAllChannels():Observable<any>{
    return this.http.get(this.chatService+"/Channels/",this.httpOpt);
  }
}