import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import{AuthService} from'./auth.service';
import {NotFoundComponent} from './not-found/not-found.component'
import { LoginAuthService } from './login-auth.service';

const routes: Routes = [
  {
    path : '',
    component : LoginComponent, 
    canActivate:[LoginAuthService] 
  },
  {
    path : 'chat-room',
    component : ChatRoomComponent,
    canActivate:[AuthService] },
  { path: '404', 
    component: NotFoundComponent},
 {  path: '**', 
    redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
