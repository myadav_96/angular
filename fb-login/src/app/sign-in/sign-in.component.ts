import { Component, OnInit } from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider
} from 'angular-6-social-login-v2';
import { Router } from '../../../node_modules/@angular/router';
import { FbService } from '../fb.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent {
  //name;
 
  constructor( private socialAuthService: AuthService,private route : Router,private fbService : FbService) {

  }
  
  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform+" sign in data : " , userData);
        //this.name = userData.image;
        this.route.navigate(["/fb-login"])
        this.fbService.fb(userData)
        
            
      }
    );
  }
  
}