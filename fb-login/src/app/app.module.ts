import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';
import { AppComponent } from './app.component';
import {
  SocialLoginModule,
  AuthServiceConfig,
  FacebookLoginProvider
} from "angular-6-social-login-v2";
import { SignInComponent } from './sign-in/sign-in.component';
import { FbLoginComponent } from './fb-login/fb-login.component';
import { FbService } from './fb.service';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("516097145502622")
        }
      ]
  );
  return config;
}

const routes : Routes = [{
  path : '',
  component : SignInComponent },
  {
    path : 'fb-login',
    component : FbLoginComponent
  }]

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    FbLoginComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    RouterModule.forRoot(routes)
  ],
  providers: [  {
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  },FbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
