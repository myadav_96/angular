import { Component, OnInit } from '@angular/core';
import { FbService } from '../fb.service';

@Component({
  selector: 'app-fb-login',
  templateUrl: './fb-login.component.html',
  styleUrls: ['./fb-login.component.css']
})
export class FbLoginComponent implements OnInit {
  fbdata;
  constructor(private fbService : FbService) {
    this.fbdata = this.fbService.getData();
   }

  ngOnInit() {
    
  }

}
